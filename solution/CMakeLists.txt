file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    include/*.h
)

add_executable(image-transformer ${sources}
        src/bmp_io.c
        include/image.h
        include/bmp_io.h
        src/transform.c
        include/transform.h
        src/handle_errors.c
        include/handle_errors.h
        include/utils.h
        include/exit_codes.h
)
target_include_directories(image-transformer PRIVATE src include)
