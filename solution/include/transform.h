//
// Created by kamil on 12.11.2023.
//
#include "image.h"

#ifndef IMAGE_TRANSFORMER_TRANSFORM_H
#define IMAGE_TRANSFORMER_TRANSFORM_H

struct image rotate90(struct image source);

struct image rotate180(struct image source);

struct image rotate270(struct image source);

#endif //IMAGE_TRANSFORMER_TRANSFORM_H
