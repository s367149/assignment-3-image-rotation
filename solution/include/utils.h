//
// Created by kamil on 13.11.2023.
//

#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H

#include <stdint.h>

struct image get_image(uint32_t width, uint32_t height);

#endif //IMAGE_TRANSFORMER_UTILS_H
